﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace UploadPie.Forms
{
    public partial class frmTemplate : Form
    {
        private Boolean dragging = false;
        private Point offset, startPoint;

        public frmTemplate()
        {
            InitializeComponent();
        }

        private void frmTemplate_Load(object sender, EventArgs e)
        {
            this.lblTitle.Text = String.Format("{0} - v{1}", Application.ProductName, Application.ProductVersion);
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            this.dragging = true;
            this.startPoint = new Point(e.X, e.Y);
        }

        private void lblTitle_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.dragging)
            {
                Point p = PointToScreen(e.Location);
                this.Location = new Point(p.X - this.startPoint.X, p.Y - this.startPoint.Y);
            }
        }

        private void lblTitle_MouseUp(object sender, MouseEventArgs e)
        {
            this.dragging = false;
        }
    }
}