﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace UploadPie.Forms
{
    public partial class frmComplete : UploadPie.Forms.frmTemplate
    {
        public frmComplete(String url, int time = 0)
        {
            InitializeComponent();
            txtSharingURL.Text = url;
            lblLinkValidTime.Text = String.Format("Link is valid for {0}.", Definitions.uploadIndexToString[time]);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void btnCopyClipboard_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtSharingURL.Text);
            btnCopyClipboard.Text = "Copied to Clipboard";
            btnCopyClipboard.ForeColor = Color.Green;
        }

        private void txtSharingURL_Click(object sender, EventArgs e)
        {
            //txtSharingURL.SelectAll();
        }

        private void frmComplete_Load(object sender, EventArgs e)
        {
        }

        private void frmComplete_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Close any hidden forms too (end application)..
            Environment.Exit(0);
        }
    }
}