﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UploadPie.Forms
{
    public partial class frmUpload : UploadPie.Forms.frmTemplate
    {
        private String filename = "";
        private String filepath = "";
        private byte[] filebytes = null;

        private DateTime lastUpdate;
        private long lastBytesSent = 0;
        private long lastPercent = 0;
        private long bytesPerSecond = 0;

        private WebClient client = null;

        // Set upload time..
        private int uploadTime = Properties.Settings.Default.DefaultUploadTime;

        public frmUpload(String filePath, bool retry = false)
        {
            InitializeComponent();

            try
            {
                this.filename = Path.GetFileName(filePath);
                this.filepath = filePath;
                this.filebytes = File.ReadAllBytes(filePath);

                verifyFile();

                this.client = new WebClient();
                uploadData();
            }
            catch (IOException e)
            {
                String msg = String.Format("An IO exception occured whilst reading the file:\r\r{0}\r\r{1}'", filePath, e.Message);
                MessageBox.Show(msg, "IO Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }
            catch (UnauthorizedAccessException)
            {
                String msg = String.Format("Unable to access the file:\r\r{0}\r\rCheck file permissions.", filePath);
                MessageBox.Show(msg, "UnauthorizedAccess Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }
        }

        private void verifyFile()
        {
            // Now determine if the path is valid by testing if the file exists..
            if (File.Exists(this.filepath) == false)
            {
                String msg = String.Format("File does not exist:\n\n\t{0}", this.filepath);
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }

            // Do we have a supported file extention?
            String extension = Path.GetExtension(this.filepath);

            if (!Properties.Settings.Default.ValidExtentions.Contains(extension))
            {
                String msg = String.Format("File extension '{0}' not supported.\n\nSupported Extentions:  {1}", extension, Properties.Settings.Default.ValidExtentions);
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }

            // Is the file 3MB or less?
            FileInfo file = new FileInfo(this.filepath);

            long filesize = file.Length;

            if (filesize > Properties.Settings.Default.MaximumFilesize)
            {
                String msg = String.Format("File is too big. 3MB (3145728 bytes) maximum.");
                MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }
        }

        private MultipartFormDataContent prepareData(String fileName, byte[] file, int expire)
        {
            MultipartFormDataContent formData = new MultipartFormDataContent("-----------------------------7df19226d09b2");

            formData.Add(new StringContent("3145728"), "MAX_FILE_SIZE");
            formData.Add(new StringContent("1"), "upload");
            formData.Add(new StringContent("116"), "x");
            formData.Add(new StringContent("11"), "y");
            formData.Add(new StringContent(expire.ToString()), "expire");
            formData.Add(new ByteArrayContent(file, 0, file.Length), "uploadedfile", fileName);

            return formData;
        }

        private async void uploadData()
        {
            // Prepare Data..
            MultipartFormDataContent formData = prepareData(this.filename, this.filebytes, uploadTime);
            byte[] data = await convertToBytes(formData);

            // Prepare WebClient..
            Uri uri = new Uri("http://uploadpie.com");

            // Prepare some headers..
            client.Headers["Content-Type"] = "multipart/form-data; boundary=-----------------------------7df19226d09b2";
            client.Headers["Referer"] = "http://uploadpie.com/";
            client.Headers["User-Agent"] = "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko";
            client.Headers["Accept-Language"] = "en-GB";
            client.Headers["Accept"] = "text/html, application/xhtml+xml, */*";
            client.Headers["Pragma"] = "no-cache";
            //client.Headers["Content-Length"] = data.Length.ToString();

            // Add event handler..
            client.UploadProgressChanged += new UploadProgressChangedEventHandler(uploadProgessCallback);
            client.UploadDataCompleted += new UploadDataCompletedEventHandler(uploadCompleteCallback);

            // Begin upload..
            client.UploadDataAsync(uri, "POST", data);
        }

        private async Task<byte[]> convertToBytes(MultipartFormDataContent data)
        {
            return await data.ReadAsByteArrayAsync();
        }

        private void uploadProgessCallback(object sender, UploadProgressChangedEventArgs e)
        {
            BeginInvoke(
                new MethodInvoker(() =>
                {
                    // Calculate our transfer speeds..
                    if (lastBytesSent == 0)
                    {
                        lastUpdate = DateTime.Now;
                        lastBytesSent = e.BytesSent;
                    }
                    var now = DateTime.Now;
                    var timeSpan = now - lastUpdate;
                    if (timeSpan.Seconds > 0)
                    {
                        var bytesChange = e.BytesSent - lastBytesSent;
                        bytesPerSecond = bytesChange / timeSpan.Seconds;
                        lastUpdate = DateTime.Now;
                        lastBytesSent = e.BytesSent;
                    }

                    // Calculate our real progress percent.
                    int realProgress = e.ProgressPercentage != 100 ? e.ProgressPercentage * 2 : e.ProgressPercentage;
                    realProgress = realProgress < 0 ? 100 : realProgress;
                    int percent = Convert.ToInt32(((e.BytesSent * 100) / e.TotalBytesToSend));

                    // Set values for progressbar, and labels.
                    this.pbUploadProgress.CurrentValue = realProgress;
                    this.lblBytes.Text = String.Format("{0} / {1} bytes         {2} kb/sec", e.BytesSent, e.TotalBytesToSend, bytesPerSecond / 1024);
                    this.lblPercent.Text = String.Format("{0}%", realProgress);

                    // Update UI/Form
                    this.Update();

                    // Displays the operation identifier, and the transfer progress.
                    // Only outputs every 10% (too much spam in debugger actually slowed the upload)
                    if (realProgress % 10 == 0 && lastPercent != realProgress)
                    {
                        lastPercent = realProgress;
                        Console.WriteLine("{0}    uploaded {1} of {2} bytes. {3} % {4} complete...",
                            (string)e.UserState,
                            e.BytesSent,
                            e.TotalBytesToSend,
                            e.ProgressPercentage,
                            realProgress);
                    }
                }));
        }

        private void uploadCompleteCallback(object sender, UploadDataCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                // Upload cancelled.. Confirm then exit.
                String msg = String.Format("The upload was cancelled.");
                MessageBox.Show(msg, "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Environment.Exit(0);
            }
            else if (e.Error != null)
            {
                // We have an error.
                if (e.Error.GetType().Name == "WebException")
                {
                    WebException we = (WebException)e.Error;
                    HttpWebResponse response = (System.Net.HttpWebResponse)we.Response;

                    String msg = String.Format("There was an error attempting to send a web request to http://uploadpie.com\r\r{0}'", we.Message);
                    MessageBox.Show(msg, "Web Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        msg = String.Format("The web server returned 'HTTP Status Code {0}'", response.StatusCode.ToString());
                        MessageBox.Show(msg, "WebServer Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                Environment.Exit(1);
            }
            else
            {
                // We should have the HTML of the result page..
                byte[] data = (byte[])e.Result;
                string reply = System.Text.Encoding.UTF8.GetString(data);

                // Define our markers..
                string start = "<input type=\"text\" id=\"uploaded\" value=\"";
                string end = "\" onclick=\"auto_select();\"";

                string s = reply;
                if (s.IndexOf(start) > 0 && s.IndexOf(end) > 0)
                {
                    // Markers are present in the HTML string, grab the string between (thats the sharing URL)..
                    s = s.Substring(s.IndexOf(start) + start.Length, s.IndexOf(end) - s.IndexOf(start) - start.Length);
                }
                else
                {
                    // Markers are not present, set null string.
                    s = null;
                }

                if (s != null)
                {
                    // We have a URL, display it to the user..
                    frmComplete frm = new frmComplete(s, uploadTime);
                    //MessageBox.Show(s);
                    frm.Show();
                    this.Hide();
                }
                else
                {
                    // No URL, show HTML output for now.
                    String msg = String.Format("Unable to obtain sharing URL.");
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Environment.Exit(1);
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            // If WebClient is busy (aka in a request..), cancel it.
            if (this.client != null && this.client.IsBusy)
            {
                this.client.CancelAsync();
            }
        }
    }
}