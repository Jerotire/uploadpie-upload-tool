﻿namespace UploadPie.Forms
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmbStorageTime = new System.Windows.Forms.ComboBox();
            this.btnExit = new UploadPie.Classes.JeroButton();
            this.btnSave = new UploadPie.Classes.JeroButton();
            this.label2 = new System.Windows.Forms.Label();
            this.btnStorageTimeTip = new UploadPie.Classes.JeroButton();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(14, 77);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 4);
            this.label1.Size = new System.Drawing.Size(80, 21);
            this.label1.TabIndex = 4;
            this.label1.Text = "Storage Time";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbStorageTime
            // 
            this.cmbStorageTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.cmbStorageTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbStorageTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cmbStorageTime.FormattingEnabled = true;
            this.cmbStorageTime.ItemHeight = 13;
            this.cmbStorageTime.Location = new System.Drawing.Point(100, 77);
            this.cmbStorageTime.Name = "cmbStorageTime";
            this.cmbStorageTime.Size = new System.Drawing.Size(264, 21);
            this.cmbStorageTime.TabIndex = 5;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(301, 150);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(90, 32);
            this.btnExit.TabIndex = 6;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(205, 150);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(90, 32);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(14, 38);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 4);
            this.label2.Size = new System.Drawing.Size(377, 21);
            this.label2.TabIndex = 8;
            this.label2.Text = "Use the settings below to configure the tool:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnStorageTimeTip
            // 
            this.btnStorageTimeTip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btnStorageTimeTip.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStorageTimeTip.Location = new System.Drawing.Point(370, 77);
            this.btnStorageTimeTip.Name = "btnStorageTimeTip";
            this.btnStorageTimeTip.Size = new System.Drawing.Size(26, 21);
            this.btnStorageTimeTip.TabIndex = 9;
            this.btnStorageTimeTip.Text = "?";
            this.btnStorageTimeTip.UseVisualStyleBackColor = false;
            this.btnStorageTimeTip.Click += new System.EventHandler(this.btnStorageTimeTip_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(415, 205);
            this.Controls.Add(this.btnStorageTimeTip);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.cmbStorageTime);
            this.Controls.Add(this.label1);
            this.Name = "frmMain";
            this.Text = "Upload Pie - Configuration";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.cmbStorageTime, 0);
            this.Controls.SetChildIndex(this.btnExit, 0);
            this.Controls.SetChildIndex(this.btnSave, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.btnStorageTimeTip, 0);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbStorageTime;
        private Classes.JeroButton btnExit;
        private Classes.JeroButton btnSave;
        private System.Windows.Forms.Label label2;
        private Classes.JeroButton btnStorageTimeTip;
    }
}
