﻿namespace UploadPie.Forms
{
    partial class frmComplete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLinkValidTime = new System.Windows.Forms.Label();
            this.txtSharingURL = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCopyClipboard = new UploadPie.Classes.JeroButton();
            this.btnExit = new UploadPie.Classes.JeroButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblLinkValidTime
            // 
            this.lblLinkValidTime.Location = new System.Drawing.Point(17, 151);
            this.lblLinkValidTime.Name = "lblLinkValidTime";
            this.lblLinkValidTime.Size = new System.Drawing.Size(168, 32);
            this.lblLinkValidTime.TabIndex = 4;
            this.lblLinkValidTime.Text = "{LINK_VALID_TIME}";
            this.lblLinkValidTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSharingURL
            // 
            this.txtSharingURL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.txtSharingURL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSharingURL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSharingURL.Font = new System.Drawing.Font("Consolas", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSharingURL.ForeColor = System.Drawing.Color.Silver;
            this.txtSharingURL.Location = new System.Drawing.Point(3, 5);
            this.txtSharingURL.Name = "txtSharingURL";
            this.txtSharingURL.ReadOnly = true;
            this.txtSharingURL.Size = new System.Drawing.Size(498, 29);
            this.txtSharingURL.TabIndex = 3;
            this.txtSharingURL.Text = "http://uploadpie.com/sgAVX";
            this.txtSharingURL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSharingURL.WordWrap = false;
            this.txtSharingURL.Click += new System.EventHandler(this.txtSharingURL_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(225, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "File Uploaded Successfully";
            // 
            // btnCopyClipboard
            // 
            this.btnCopyClipboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btnCopyClipboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCopyClipboard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCopyClipboard.Location = new System.Drawing.Point(320, 152);
            this.btnCopyClipboard.Name = "btnCopyClipboard";
            this.btnCopyClipboard.Size = new System.Drawing.Size(167, 31);
            this.btnCopyClipboard.TabIndex = 1;
            this.btnCopyClipboard.Text = "&Copy to Clipboard";
            this.btnCopyClipboard.UseVisualStyleBackColor = true;
            this.btnCopyClipboard.Click += new System.EventHandler(this.btnCopyClipboard_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(496, 152);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(90, 32);
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.panel5.Controls.Add(this.txtSharingURL);
            this.panel5.Location = new System.Drawing.Point(48, 80);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.panel5.Size = new System.Drawing.Size(504, 37);
            this.panel5.TabIndex = 5;
            // 
            // frmComplete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(600, 200);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.lblLinkValidTime);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCopyClipboard);
            this.Controls.Add(this.btnExit);
            this.Name = "frmComplete";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmComplete_FormClosing);
            this.Load += new System.EventHandler(this.frmComplete_Load);
            this.Controls.SetChildIndex(this.btnExit, 0);
            this.Controls.SetChildIndex(this.btnCopyClipboard, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lblLinkValidTime, 0);
            this.Controls.SetChildIndex(this.panel5, 0);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UploadPie.Classes.JeroButton btnExit;
        private UploadPie.Classes.JeroButton btnCopyClipboard;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSharingURL;
        private System.Windows.Forms.Label lblLinkValidTime;
        private System.Windows.Forms.Panel panel5;
    }
}
