﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace UploadPie.Forms
{
    public partial class frmMain : UploadPie.Forms.frmTemplate
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            // Populate StorageTime combobox..
            foreach (String item in Definitions.uploadIndexToString)
            {
                if (item != "Unknown")
                {
                    this.cmbStorageTime.Items.Add(item);
                }
            }
            cmbStorageTime.SelectedIndex = Properties.Settings.Default.DefaultUploadTime - 1;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.DefaultUploadTime = cmbStorageTime.SelectedIndex + 1;
            Properties.Settings.Default.Save();

            this.btnSave.ForeColor = Color.LimeGreen;
            this.btnSave.Text = "Saved";
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void btnStorageTimeTip_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This value indicates the time your file lives for on Upload Pie's servers.", "Tip: Storage Time", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}