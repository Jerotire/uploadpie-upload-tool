﻿using System;
using System.Windows.Forms;

namespace UploadPie
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            // Get arguments..
            String[] args = Environment.GetCommandLineArgs();

            // Determine if the application was launched to upload or configure..
            if (args.Length < 2)
            {
                // Application was launched to configure.

                // Start application, load frmMain.
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Forms.frmMain());
            }
            else
            {
                // Application was launched with additional arguments..
                String action = args[1];

                // Determine if the action.
                switch (action)
                {
                    case "upload":
                        // Get secondary argument if it exists..
                        if (args.Length < 3)
                        {
                            String msg = String.Format("Missing Argument (arg2 : filePath)");
                            MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Environment.Exit(1);
                            break;
                        }

                        String filePath = args[2];

                        // Proceed to opening the upload form.
                        Application.EnableVisualStyles();
                        Application.SetCompatibleTextRenderingDefault(false);
                        Application.Run(new Forms.frmUpload(filePath));
                        break;

                    default:
                        String msg1 = String.Format("Application was launched with an invalid argument (arg1 : action)");
                        MessageBox.Show(msg1, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Environment.Exit(1);
                        break;
                }
            }
        }
    }
}