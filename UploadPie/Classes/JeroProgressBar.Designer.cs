﻿namespace UploadPie.Classes
{
    partial class JeroProgressBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBarBG = new System.Windows.Forms.Panel();
            this.pnlBarFG = new System.Windows.Forms.Panel();
            this.pnlBarBG.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBarBG
            // 
            this.pnlBarBG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.pnlBarBG.Controls.Add(this.pnlBarFG);
            this.pnlBarBG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBarBG.Location = new System.Drawing.Point(0, 0);
            this.pnlBarBG.Name = "pnlBarBG";
            this.pnlBarBG.Padding = new System.Windows.Forms.Padding(2);
            this.pnlBarBG.Size = new System.Drawing.Size(573, 24);
            this.pnlBarBG.TabIndex = 1;
            // 
            // pnlBarFG
            // 
            this.pnlBarFG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pnlBarFG.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlBarFG.Location = new System.Drawing.Point(2, 2);
            this.pnlBarFG.Name = "pnlBarFG";
            this.pnlBarFG.Size = new System.Drawing.Size(296, 20);
            this.pnlBarFG.TabIndex = 0;
            // 
            // JeroProgressBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlBarBG);
            this.Name = "JeroProgressBar";
            this.Size = new System.Drawing.Size(573, 24);
            this.Load += new System.EventHandler(this.JeroProgressBar_Load);
            this.pnlBarBG.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlBarBG;
        private System.Windows.Forms.Panel pnlBarFG;
    }
}
