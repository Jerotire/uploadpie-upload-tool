﻿using System.Drawing;
using System.Windows.Forms;

namespace UploadPie.Classes
{
    public partial class JeroButton : Button
    {
        public JeroButton()
        {
            this.BackColor = Color.FromArgb(1, 10, 10, 10);
            this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        }
    }
}