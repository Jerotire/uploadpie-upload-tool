﻿using System;
using System.Windows.Forms;

namespace UploadPie.Classes
{
    public partial class JeroProgressBar : UserControl
    {
        private int minValue;
        private int maxValue;
        private int curValue;

        public JeroProgressBar()
        {
            InitializeComponent();

            this.Resize += new EventHandler(JeroProgressBar_Resize);

            minValue = 0;
            maxValue = 100;
            curValue = 0;

            UpdateUI();
        }

        private void JeroProgressBar_Load(object sender, EventArgs e)
        {
            UpdateUI();
        }

        public int Increment(int step = 1)
        {
            if (this.curValue < this.maxValue)
            {
                this.curValue += step;
                UpdateUI();
            }
            return this.curValue;
        }

        public int MinimumValue
        {
            get
            {
                return this.minValue;
            }
            set
            {
                if (value >= 0 && value < int.MaxValue)
                {
                    if (value >= this.maxValue)
                    {
                        throw new ArgumentOutOfRangeException("MinimumValue", "Cannot set value higher or equal to MaximumValue.");
                    }
                    else
                    {
                        this.minValue = value;
                    }
                }
                else
                {
                    throw new ArgumentOutOfRangeException("MinimumValue", "Out of range: Specify an int between 0 and " + int.MaxValue.ToString());
                }
            }
        }

        public int MaximumValue
        {
            get
            {
                return this.maxValue;
            }
            set
            {
                if (value >= 1 && value < int.MaxValue)
                {
                    if (value <= this.minValue)
                    {
                        throw new ArgumentOutOfRangeException("MaximumValue", "Cannot set value lower or equal to MinimumValue.");
                    }
                    else
                    {
                        this.maxValue = value;
                    }
                }
                else
                {
                    throw new ArgumentOutOfRangeException("MaximumValue", "Out of range: Specify an int between 1 and " + int.MaxValue.ToString());
                }
            }
        }

        public int CurrentValue
        {
            get
            {
                return this.curValue;
            }
            set
            {
                if (value >= this.minValue && value <= int.MaxValue)
                {
                    this.curValue = value;
                    UpdateUI();
                }
                else
                {
                    throw new ArgumentOutOfRangeException("CurrentValue", "Out of range: Specify an int between MinimumValue and MaximumValue");
                }
            }
        }

        public int Percentage
        {
            get
            {
                return Convert.ToInt32((this.curValue * 100M / this.maxValue));
            }
        }

        public int PnlWidth
        {
            get
            {
                return this.pnlBarFG.Width;
            }
        }

        public void UpdateUI()
        {
            this.pnlBarFG.Width = Convert.ToInt32(((this.pnlBarBG.Width - this.pnlBarBG.Padding.Left - this.pnlBarBG.Padding.Right) / 100M * this.Percentage));
        }

        private void JeroProgressBar_Resize(object sender, EventArgs e)
        {
            UpdateUI();
        }
    }
}