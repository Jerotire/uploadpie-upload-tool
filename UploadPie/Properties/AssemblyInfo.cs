﻿using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Upload Pie: Upload Tool")]
[assembly: AssemblyDescription("This tool allows users to quickly upload their files to Upload Pie.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SKYCMD Software")]
[assembly: AssemblyProduct("Upload Pie: Upload Tool")]
[assembly: AssemblyCopyright("Copyright ©  2015 Jerotire")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("977b1e6c-bac2-4bcb-8678-c6c7bfa54bab")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.0.0.1")]
[assembly: AssemblyFileVersion("0.0.0.1")]
[assembly: NeutralResourcesLanguage("en-GB")]
[assembly: InternalsVisibleTo("UploadPie.Tests")]
[assembly: InternalsVisibleTo("UploadPie.Explorables")]