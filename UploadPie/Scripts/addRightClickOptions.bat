@echo off
SET exepath=%~dp0UploadPie.exe
SET extensions=.txt,.png,.gif,.pdf,.jpg
setlocal

echo //////////////////////////////////////////////////////
echo UploadPie Tool - Shell Integration (INSTALLATION)
echo ------------------------------------------------------
echo This batch file will edit your registry:
echo   - Make sure you backed up registry..
echo //////////////////////////////////////////////////////

@echo off
openfiles > NUL 2>&1 
if NOT %ERRORLEVEL% EQU 0 goto NOTADMIN 
echo You're running the script as Administrator. Great!

:PROMPT
SET /P AREYOUSURE=Do you wish to continue? (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END

echo Great! Let's start..
echo ------------------------------------------------------

for %%a in ("%extensions:,=" "%") do (
	echo # Adding entry for %%a
	reg add HKEY_CLASSES_ROOT\SystemFileAssociations\\%%a\shell\UploadPie /ve /d "Upload Pie" /f
	reg add HKEY_CLASSES_ROOT\SystemFileAssociations\\%%a\shell\UploadPie /v Icon /t REG_SZ /d "%exepath%" /f
	reg add HKEY_CLASSES_ROOT\SystemFileAssociations\\%%a\shell\UploadPie\command /ve /d "%exepath% upload \"%%1\"" /f
)

:NOTADMIN
echo You must run this bat file as Administrator.

:END
echo //////////////////////////////////////////////////////
echo Script End
endlocal