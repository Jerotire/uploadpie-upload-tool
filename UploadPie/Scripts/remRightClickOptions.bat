@echo off
SET exepath=%~dp0..\bin\Release\UploadPie.exe
SET extensions=.txt,.png,.gif,.pdf,.jpg
setlocal

echo //////////////////////////////////////////////////////
echo UploadPie Tool - Shell Integration (REMOVAL)
echo ------------------------------------------------------
echo This batch file will edit your registry:
echo   - Make sure you backed up registry..
echo //////////////////////////////////////////////////////

@echo off
openfiles > NUL 2>&1 
if NOT %ERRORLEVEL% EQU 0 goto NOTADMIN 
echo You're running the script as Administrator. Great!

:PROMPT
SET /P AREYOUSURE=Do you wish to continue? (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END

echo Great! Let's start..
echo ------------------------------------------------------

for %%a in ("%extensions:,=" "%") do (
	echo # Removing entry for %%a
	reg delete HKEY_CLASSES_ROOT\SystemFileAssociations\\%%a\shell\UploadPie /f
)

:NOTADMIN
echo You must run this bat file as Administrator.

:END
echo //////////////////////////////////////////////////////
echo Script End
endlocal