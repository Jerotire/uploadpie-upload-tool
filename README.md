# Upload Pie: Upload Tool #

**Welcome to Upload Pie: Upload Tool**

This tool allows users to quickly upload their files to Upload Pie.

They accomplish this by simply by right clicking a supported file (.txt, .png, .pdf, .gif, .jpg) in Windows Explorer, and selecting Upload Pie. Providing that the file is not over 3MB (megabyte), the file will be uploaded to [Upload Pie](http://uploadpie.com/) whilsts showing its current progress; when the upload is completed, the user will be presented with a sharing link.

It is developed using Microsoft Visual Studio 2013/15 (C#)

## Testing/Installing ##

### Simple Deployment: ###
1. Visit the project's [download page](https://bitbucket.org/Jerotire/uploadpie-upload-tool/downloads).
2. Download and run the installer.

Done!

### From Source: ###
1. Download source, and build.
2. Copy the built executable, and the scripts (**addRightClickOptions.bat** and **remRightClickOptions.bat**) from the Scripts directory into a new folder.
3. Move that folder where you need/want it to be.
3. Run the script **addRightClickOptions.bat** (ensure you run it in an elevated Command Prompt).

Done!

**Pro Tip:** *You can continue to build the application, and the new executable can just be copied into your folder (overwriting existing executable).*

### Tests ###

The application has been tested in the following environments:

* Windows 8.1 (x64)
* Windows 10 (x64)